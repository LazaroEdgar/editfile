import com.spire.presentation.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class EditPpt {
    public static  void run(String address_file,String address_save,List<String[]> values) throws  Exception{
        Presentation presentation = new Presentation();
        presentation.loadFromFile(address_file,FileFormat.PPTX_2013);
        for(int i=0;i<presentation.getSlides().getCount();i++){
            ISlide slide= presentation.getSlides().get(i);
            replaceText(slide,values);
        }
        presentation.saveToFile(address_save,FileFormat.PDF);
        //presentation.saveToFile(address_save, FileFormat.PPTX_2013);
    }
    public static void main(String[] args) throws Exception {

        List<String[]> map = new ArrayList<>();
        map.add(new String[]{"CAYETANO ZUNIGA JUAN CARLOS","VAMOS TODAX A VER QUE PASA"});
        File af = new File("C:\\Users\\lenovo\\Desktop\\nuevo.pptx");
        if (af.exists())
        run(af.getPath(),"output/ReplaceText.pdf",map);
        else
            System.out.println("No existe este archivo");
    }
    /*
     * Replace text within a slide
     * @param slide Specifies the slide where the replacement happens
     * @param map Where keys are existing strings in the document and values are the new strings to replace the old ones
     */
    public static void replaceText(ISlide slide,List<String[]> map) {
        for (Object shape : slide.getShapes())
            if (shape instanceof IAutoShape)
                for (Object paragraph : ((IAutoShape) shape).getTextFrame().getParagraphs()) {
                    ParagraphEx paragraphEx =(ParagraphEx)paragraph;
                    for (String[] value : map)
                        if (paragraphEx.getText().contains(value[0]))
                            paragraphEx.setText(paragraphEx.getText().replace(value[0],value[1]));
                }
    }

}